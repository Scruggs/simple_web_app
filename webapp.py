import http.server
import socketserver
import os
import cases
import subprocess


class RequestHandler(http.server.SimpleHTTPRequestHandler):
    '''Handle HTTP requests by returning a fixed 'page'.'''

    Listing_Page = '''\
<html>
<body>
<ul>
{0}
</ul>
</body>
</html>
'''

    Error_Page = """\
<html>
<body>
<h1>Error accessing {path}</h1>
<p>{msg}</p>
</body>
</html>
"""

    Cases = [cases.case_no_file,
             cases.case_cgi_file,
             cases.case_existing_file,
             cases.case_directory_index_file,
             cases.case_directory_no_index_file,
             cases.case_always_fail]

    # Handle a GET request.
    def do_GET(self):
        try:

            # Figure out what exactly is being requested.
            self.full_path = os.getcwd() + self.path
            # Figure out how to handle it.
            for case in self.Cases:
                handler = case()
                if handler.test(self):
                    handler.act(self)
                    break

        # Handle errors.
        except Exception as msg:
            self.handle_error(msg)

    def handle_error(self, msg):
        content = self.Error_Page.format(path=self.path, msg=msg)
        self.send_content(content, 404)

    def send_content(self, content, status=200):
        self.send_response(status)
        self.send_header("Content-type", "text/html")
        self.send_header("Content-Length", str(len(content)))
        self.end_headers()
        self.wfile.write(content)

    def list_dir(self, full_path):
        try:
            entries = os.listdir(full_path)
            bullets = ['<li>{0}</li>'.format(e) 
                for e in entries if not e.startswith('.')]
            page = self.Listing_Page.format('\n'.join(bullets))
            self.send_content(page)
        except OSError as msg:
            msg = "'{0}' cannot be listed: {1}".format(self.path, msg)
            self.handle_error(msg)

    def run_cgi(self, full_path):
        cmd = "python " + full_path
        p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)
        p.stdin.close()
        data = p.communicate()[0]
        p.stdout.close()
        self.send_content(data)


if __name__ == '__main__':
    print(os.getpid())
    serverAddress = ('', 8080)
    server = socketserver.TCPServer(("127.0.0.1", 8180), RequestHandler)
    server.serve_forever()  

